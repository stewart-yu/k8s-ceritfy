## 黄云使用本地local-up方法对k8s版本认证流程指导

#### 把相关测试yaml文件拉取到本地路径

```shell
wget https://raw.githubusercontent.com/cncf/k8s-conformance/master/sonobuoy-conformance-1.7.yaml
```

#### 关闭除docker外所有代理

验证当前环境是否还存在相关代理：

```shell
export
```

### 切换到项目路径

(1) 检查项目路径是否还存在未提交的文件。如果存在，请参考步骤（2），否则请直接至（3）。

```shell
git status
```

(2) 提交相关未提交代码

```shell
git add <file-name>
git commit -m "some explain"
```

(3) 检查版本tag是否规范（这一步骤可当测试用例报错再操作）
   版本tag建议使用如下格式：
  
     `<k8s开源基线版本号>-<私有版本标记段>`
     
   请按如下操作修改版本tag：
   
   ```shell
   git tag <newtag-name>
   make clean
   ```
    
(4) 本地local-up拉起本地集群
 切换至项目路径

```shell
export ALLOW_PRIVILEGED=true
export ALLOW_SECURITY_CONTEXT=false
make
mkdir -p /var/run/kubernetes/
rm -rf /var/run/kubernetes/*
rm -rf /tmp/*
hack/local-up-cluster.sh
```

### 拷贝kubectl接口到PATH路径

如： `cp path/kubectl /usr/local/bin`

现在，你可以在任何地方使用Kubectl命令。

### 打开另外一个窗口
(1) 切换到sonobuoy-conformance-1.7.yaml所在路径

(2) 创建e2e pod 和 sonobuoy master, 一致性测试由这两个pod协作完成

```shell
kubectl apply -f sonobuoy-conformance-1.7.yaml
```

(3) 查看pod `sonobuoy`记录，等待运行结束

```shell
kubectl logs -f -n sonobuoy sonobuoy 
```

直到出现'wait for the line no-exit was specified, sonobuoy is now blocking'，则说明测试完成。正常55分钟左右。

（4）导出测试结果文件

```shell
kubectl cp sonobuoy/sonobuoy:/tmp/sonobuoy /home/result
```

(5) 清理测试资源

```shell
kubectl delete -f sonobuoy-conformance-1.7.yaml
```

(6)切换到结果目录并解压文件

```shell
cd /home/result
tar -xzf *_sonobuoy_*.tar.gz
```

获取plugins/e2e/results/{e2e.log,junit_01.xml}。

当前，cncf指导拉取的yaml创建的pod运行的是社区k8s v1.7的测试用例，所以在黄云下会出现一个测试用例失败：
`[Fail] [k8s.io] Networking [BeforeEach] should provide Internet connection for containers [Conformance]`
在之后的版本，cncf会使用最新k8s版本的测试用例，进而解决这个问题。

